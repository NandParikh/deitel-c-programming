// C How to Program 7/E Deitel
// Program 2.32
// "Body mass Index Calculator"

#include<stdio.h>
#include<conio.h>

int main()
{
    float BMI;
    float weight, height;
    int group;

    printf("\n ******* BODY MASS INDEX CALCULATOR *******\n");
    printf("\n Enter your weight<kg> : ");
    scanf("%f", &weight);
    printf("\n Enter your height<cm> : ");
    scanf("%f", &height);

    BMI = (weight) / ((height/100)* (height/100));

    if(BMI < 18.5) {group = 1;}
    if(BMI >= 18.5 && BMI <= 24.9 ){group = 2;}
    if(BMI > 24.9 && BMI <= 29.9) {group = 3;}
    if(BMI >= 30) {group = 4;}

    printf("\n****************");
    printf("\n RESULTS :-");
    printf("\n****************");

    switch(group)
    {
        case 1 : printf("\n Your BMI is %.2f and your class is 'Underweight'!!!\n", BMI);
                 printf("\n Add Healthy snacks to your diet :) \n");
                 break;

        case 2 : printf("\n Your BMI is %.2f and your class is 'Normal'!!!\n", BMI);
                 printf("\n Stay as you are :) \n");
                 break;

        case 3 : printf("\n Your BMI is %.2f and your class is 'Overweight'!!!\n", BMI);
                 printf("\n Do Exercises, Start slow then go intense :) \n");
                 break;

        case 4 : printf("\n Your BMI is %.2f and your class is 'Obese'!!!\n", BMI);
                 printf("\n Consult a doctor as soon as you can!!\n");
                 break;

        default : printf("\n OOPS..!! It looks like something went wrong! TRY AGAIN :)");
                  break;

    }

    return 0;
}
