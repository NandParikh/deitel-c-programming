# README #

### What is this repository for? ###

* **This repository is for "Deitel C How to Program" 7th Edition "Making a Difference" exercises. :)**
* Version 1.0a
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How to run codes? ###

* You can either use 'Turbo C++' or 'CodeBlocks'.
* *Turbo C++* : http://sourceforge.net/projects/turbocforwindows-9/files/latest/download
* *CodeBlocks* : http://sourceforge.net/projects/codeblocks/files/Binaries/13.12/Windows/codeblocks-13.12mingw-setup.exe/download 

### Contribution guidelines ###

* Help is appreciated. :) 
* **Making the code smaller and more efficient is the goal.** 
* Either commit OR PM me.

### Who do I talk to? ###

* Repo owner **[NandParikh]**