// C How to Program 7/E Deitel
// Program 3.46
// "Body mass Index Calculator"

#include<stdio.h>
#include<conio.h>

/* Start of the program*/
int main()
{
        int choice;                                     // Declaring variables
        long long int world_population = 7288900000;    // Reference taken from www.worldometers.info/world-population/
        float world_growth = 1.14;

        while(1)                        // Creates infinite loop0
        {
            printf("\n ******* World Population Growth Calculator *******");
            printf("\n\n Right now the world population is : %lld", world_population);
            printf("\n The annual growth rate : %.2f %", world_growth);
            printf("\n Want to know the estimated population in future? Check it out!!");
            printf("\n\n Choices :-");
            printf("\n 1. In 1 year");
            printf("\n 2. In 2 years");
            printf("\n 3. In 3 years");
            printf("\n 4. In 4 years");
            printf("\n 5. In 5 years");
            printf("\n 6. Exit the program\n");
            printf("\n Enter your choice : ");
            scanf("%d", &choice);

            switch(choice)
            {
                case 1:{
                         long long int world_population = 7288900000;
                         world_population = world_growth * (choice + 1) * world_population;
                         printf("\n Processing......");
                         printf("\n ******************");
                         printf("\n After 1 year");
                         printf("\n ******************");
                         printf("\n The world population will be %lld !\n", world_population);
                         break;
                }

                case 2:{
                         long long int world_population = 7288900000;
                         world_population = world_growth * (choice + 1) * world_population;
                         printf("\n Processing......");
                         printf("\n ******************");
                         printf("\n After 2 years");
                         printf("\n ******************");
                         printf("\n The world population will be %lld !!\n", world_population);
                         break;
                }

                case 3:{
                         long long int world_population = 7288900000;
                         world_population = world_growth * (choice + 1) * world_population;
                         printf("\n Processing......");
                         printf("\n ******************");
                         printf("\n After 3 years");
                         printf("\n ******************");
                         printf("\n The world population will be %lld !!!\n", world_population);
                         break;
                }

                case 4:{
                         long long int world_population = 7288900000;
                         world_population = world_growth * (choice + 1) * world_population;
                         printf("\n Processing......");
                         printf("\n ******************");
                         printf("\n After 4 years");
                         printf("\n ******************");
                         printf("\n The world population will be %lld !!!!\n", world_population);
                         break;
                }

                case 5:{
                         long long int world_population = 7288900000;
                         world_population = world_growth * (choice + 1) * world_population;
                         printf("\n Processing......");
                         printf("\n ******************");
                         printf("\n After 5 years");
                         printf("\n ******************");
                         printf("\n The world population will be %lld !!!!!\n", world_population);
                         break;
                }

                case 6 : return 0;

                default : system("cls");
                          break;


            }               // End of Switch


        }                   // End of while loop

    return 0;

}                           // End of main program
